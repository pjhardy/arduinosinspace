# Arduinos In Space!

A library for interacting with [Objects in Space](http://objectsgame.com/)
via a serial connection.

## Protocol support

Arduinos In Space understands all of the known requests and commands
generated in `serial_commands.txt` by Objects In Space 1.0.7. The protocol,
and requests and commands, are still being added and refined. Arduinos In
Space may lag slightly, or even briefly stop working between releases.

### Compatibility chart

In general, make sure you have the most recent versions of the game and
this library. But at the very least, refer to the below table to ensure
that you're using a version of the library that's definitely compatible
with the game version you're using. I can't provide support if not.

|Arduinos in Space | Objects in Space|
|------------------|-----------------|
| 1.0 - 1.0.1      | 1.0 - 1.0.2     |
| 1.1 - 1.2        | 1.0.3 - 1.0.7   |

## Installation

### Arduino IDE

Open the Arduino Library Manager (Tools menu -> Manage Libraries...). Search
for "ArduinosInSpace". Hit the install button.

### PlatformIO

[pio lib install "ArduinosInSpace"](https://platformio.org/lib/show/5652/ArduinosInSpace)

### Manually

Grab the latest release zip file from
https://bitbucket.org/pjhardy/arduinosinspace/downloads/

## Documentation

Full documentation, including usage and the API, is available at the
OiS wiki: https://oiswiki.sysadninjas.net/wiki/Arduinos_in_Space

### Example sketches

Sketches in the `examples/` directory illustrate basic usage for trivial
controllers.

## Discussion

Drop by the [Arduinos In Space](http://forum.objectsgame.com:88/t/arduinos-in-space-arduino-library-for-ois-controllers/1315)
thread on the [Objects in Space forum](http://forum.objectsgame.com:88/).
