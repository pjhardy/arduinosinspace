# Parses the serial_commands.txt file produced by Objects in Space
# to produce an ArduinosInSpaceRequests.h header file for Arduinos
# in Space. Run it like this:
# awk -f parseserial.awk < serial_commands.txt > ArduinosInSpaceRequests.h

BEGIN {
    print("// This file was generated from serial_commands.txt output by")
    print("// Objects in Space, using extras/parseserial.awk.")
    print("#ifndef ArduinosInSpaceRequests_h")
    print("#define ArduinosInSpaceRequests_h")
    print("")
    print("const OISCommand NULL_COMMAND = {0, \"\"};")
    print("")
    request_idx=1
    command_idx=1
    state=""
}

/^SERIAL OUTPUT COMMAND LIST/ {
    version = $3
}

/^Numerical commands:/ {
    print("/***********************************************************")
    print("                     Numerical requests")
    print("                    (NIN or NIF requests)")
    print("***********************************************************/")
    state="numerical_commands"
}

/^Boolean check functions:/ {
    print("")
    print("/***********************************************************")
    print("                      Boolean requests")
    print("                       (NIB requests)")
    print("***********************************************************/")
    state="boolean_check_functions"
}

/^Ship commands:/ {
    print("")
    print("")
    print("/***********************************************************")
    print("                       Ship commands")
    print("***********************************************************/")
    state="ship_commands"
}

/^COMMAND:/ {
    commandtxt = $2
    if (state == "ship_commands") {
	print("const OISCommand "commandtxt" = {"command_idx", \""commandtxt"\"};")
	command = ""
	command_idx = command_idx+1
    }	
}

/^FUNCTION:/ {
    functiontxt = $2
}

/^DESCRIPTION:/ {
    $1 = ""
    description = $0
    if (state == "numerical_commands") {
	print("//"description)
	print("const OISCommand "commandtxt" = {"request_idx", \""commandtxt"\"};")
	commandtxt = ""
    } else if (state == "boolean_check_functions") {
	print("//"description)
	print("const OISCommand "functiontxt" = {"request_idx", \""functiontxt"\"};")
	functiontxt = ""
    }
    commandtxt = ""
    functiontxt = ""
    request_idx = request_idx + 1
}

END {
    print("")
    print("#endif")
}
