# Protocol notes

Addenda, errata, and miscellena around the
[serial data protocol documentation](http://objectsgame.com/the-controllers/ois-serial-data-protocol/). This covers version 1 of the protocol, as it's
implemented in current Early Access releases of the game.

## Handshaking

To begin handshaking, the client should send `"451\n"`. A successful
acknowledgement from the server will be the string `"452\r\n"` (**note:**
this is a newline and linefeed, not just `\n`).

## Synchronisation

Client commands are the reverse of what is in the documentation:

```
CMD=COMMAND_NAME,channel_id
```

The example given in the documentation should really be sent as

```
CMD=BURN_MAIN_ENGINE,4
```

## Requests and commands

ArduinosInSpace implements the
[commands](https://oiswiki.sysadninjas.net/wiki/Serial_Commands) and
[requests](https://oiswiki.sysadninjas.net/wiki/Serial_Requests) documented
in the
[Unofficial Objects In Space wiki](https://oiswiki.sysadninjas.net/wiki/Main_Page).
The dev team have
[indicated](http://forum.objectsgame.com/t/ois-arduino-code-during-beta/911/29)
that future releases will include up-to-date
dumps of the understood verbs. I'll switch to those as soon as they're
released.
