/* HelloWorld
   Getting started with ArduinosInSpace.
   This sketch walks through basic ArduinosInSpace usage. We'll
   set the library up, connect to the game, and request information
   about EMCON status. We'll use that status to light the Arduino's
   build-in LED when EMCON is active.

   Hardware:
   - An Arduino-compatible board. That's it!

   Adapted from code contributed by smt923;
   https://github.com/smt923/LED-in-space
*/
#include "ArduinosInSpace.h"

// Create an ObjectsInSpace object. The arguments are the
// serial device to use (use "Serial" if unsure), and the
// number of request channels we will use.
// We're going to request a single channel, EMCON mode.
ObjectsInSpace OIS(Serial, 1);

void setup() {
  // We're using LED_BUILTIN, which is an alias for the
  // pin with the Arduino's built-in LED (usually pin 13)
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);

  // Open the serial connection.
  Serial.begin(9600);
  // Tell our ArduinosInSpace object to perform handshaking.
  OIS.begin();

  // Synchronise with the game.
  // Here we request information about EMCON status.
  // EMCON status is sent as a boolean, so we need
  // to use registerBool to request it.
  OIS.registerBool(EMCON_MODE, emconCallback);

  // Complete synchronisation and activate the game link.
  OIS.activate();
}

void loop() {
  // Asks our ArduinosInSpace object to process data
  // received from the game. This needs to be called
  // frequently.
  OIS.update();
}

// This is the callback function we registered in setup().
// ArduinosInSpace executes this function when EMCON
// data is received.
void emconCallback(int channel, bool data) {
  if (data == false) {
    // Normal operation
    digitalWrite(LED_BUILTIN, LOW);
  }
  else {
    // EMCON mode is active
    flashLED(3);
    digitalWrite(LED_BUILTIN, HIGH);
  }
}

// Simple function to flash our LED 'num' times.
// Note that this function is blocking - our controller
// won't be able to receive updates or perform any
// other tasks while the LED is flashing.
void flashLED(int num) {
  for (int i=0; i<=num; i++) {
    digitalWrite(LED_BUILTIN, LOW);
    delay(50);
    digitalWrite(LED_BUILTIN, HIGH);
    delay(100);
  }
}
