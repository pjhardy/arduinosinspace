/* OiSLCD
   This example displays telemetry from the game on an LCD. It uses
   the built-in LiquidCrystal library - see
   https://www.arduino.cc/en/Reference/LiquidCrystal for details.

   Output is formatted to fit on a 16x2 display, but will work unchanged
   on larger displays, and locations can be tweaked to fit on to
   smaller displays.

   If this is your first time using the LiquidCrystal library, it's
   recommended to try out the HelloWorld tutorial at
   https://www.arduino.cc/en/Tutorial/HelloWorld . This will ensure
   that your display is connected and working properly. This demo
   uses the same connections as that tutorial.

   Hardware: A 16x2 LCD display.
   See https://www.arduino.cc/en/Tutorial/HelloWorld for wiring
   directions and schematic.

   This sketch contributed by LeahNudle: https://github.com/nudle/OiSLCD
*/
#include <LiquidCrystal.h>
#include <ArduinosInSpace.h>

// Initialise the LiquidCrystal library
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

// Create an ObjectsInSpace object. The first parameter is the
// serial interface to use. The second is the number of values
// we're requesting from the game.
ObjectsInSpace OIS(Serial, 4);

void setup() 
{
  // set up the LCD's number of columns and rows:
  // specific for a 16x2 LCD screen (change this for different sizes)
  lcd.begin(16, 2);

  Serial.begin(9600); //must be 9600
  OIS.begin(); //begin the handshake/synchronisation with the game

  // handshake
  // register the command to get the speed from the game.
  // check below for the callback
  OIS.registerFloat(CURRENT_SPEED, speedCallback);
  // same for the current direction/heading, as an int
  OIS.registerInt(DIRECTION, headingCallback);
  // same for the power level percentage
  OIS.registerInt(POWER_LEVEL, powerCallback);
  // same for whether the main engine is burning, a bool
  OIS.registerBool(MAIN_ENGINE_BURNING, burningCallback);

  OIS.activate(); //stop syncing and ACTIVATE
}

void loop() 
{
  OIS.update(); //required to keep getting info from the game.
}

// because we registered this in setup(), this gets called every time
// OIS.update() is called. therefore our info is refreshed
void speedCallback(int channel, float data)
{                               //^ we registered a float so
                                //this needs to be a float
  lcd.setCursor(0,0); //set the cursor to the top left of the screen (0,0)
  lcd.print("SPD:"); //print something to the screen! this automatically moves
  lcd.print(data);   //the cursor so we dont need to move it again before the
                     //next printout
}

void headingCallback(int channel, int data)//same as above but for an int
{                                 
  lcd.setCursor(0, 1); //set the cursor to the second row
  lcd.print("HDG:");
  lcd.print(data);
}

void powerCallback(int channel, int data)
{
  lcd.setCursor(8, 0); //set the cursor to the 9th position of the first row
  lcd.print("PWR:");
  lcd.print(data);
}

void burningCallback(int channel, bool data)
{
  lcd.setCursor(15, 0);
  if(data == true)
  {
    lcd.print("#");
  }
  else
  {
    lcd.print(".");
  }
}