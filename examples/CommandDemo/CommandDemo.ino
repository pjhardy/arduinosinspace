/* CommandDemo
   A demo of sending commands using ArduinosInSpace. Here we're using
   a big red button to control the main engine. Hold the button down
   to burn.

   Before experimenting with this sketch, please make sure you're
   familiar with the official Arduino Debounce tutorial, at
   https://www.arduino.cc/en/Tutorial/Debounce

   This example requires the same hardware setup as the Debounce
   tutorial. We then build off the Debounce sketch, adding
   in ArduinosInSpace to send our debounced button presses
   to the game rather than light up an LED.
*/
#include <ArduinosInSpace.h>

// The pin our button is connected to
const int engineButtonPin = 2;

int buttonState;           // The current reading from the input pin
int lastButtonState = LOW; // The previous reading from the input pin

// The last time the "debounced" button state changed
unsigned long lastDebounceTime = 0;
// How long the state must stay the same before being considered debounced
unsigned long debounceDelay = 50;

// Create an ObjectsInSpace object. The arguments are
// the serial device to use (use "Serial" if unsure),
// and the number of request channels. We're not
// requesting any data, so that number is 0
ObjectsInSpace OIS(Serial, 0);

void setup() {
  pinMode(engineButtonPin, INPUT);

  // Open the serial connection, and tell ArduinosInSpace
  // to perform handshaking with the game
  Serial.begin(9600);
  OIS.begin();

  // Synchronise with the game. Here we tell it that
  // we'll be sending main engine start and stop commands.
  OIS.registerCommand(BURN_MAIN_ENGINE);
  OIS.registerCommand(STOP_MAIN_ENGINE);

  // Complete synchronisation and activate the game link.
  OIS.activate();
}

void loop() {
  // Read the state of the button in to a local variable
  int reading = digitalRead(engineButtonPin);

  // If the switch changed, either due to noise or being pressed/released
  if (reading != lastButtonState) {
    // reset the debounce timer
    lastDebounceTime = millis();
  }

  if ((millis() - lastDebounceTime) > debounceDelay) {
    // The reading has stayed the same for longer than the debounce
    // delay, so it's safe to assum that's the actual current state

    // If the button state has changed
    if (reading != buttonState) {
      // Update the button state
      buttonState = reading;
      if (buttonState == HIGH) {
	// The button has just been pressed, so burn the engine
	OIS.sendCommand(BURN_MAIN_ENGINE);
      } else {
	// The button has just been released, so stop burning
	OIS.sendCommand(STOP_MAIN_ENGINE);
      }
    }
  }
  // Save the button reading.
  lastButtonState = reading;

  // Finally, let ArduinosInSpace check for and process new data.
  OIS.update();
}
