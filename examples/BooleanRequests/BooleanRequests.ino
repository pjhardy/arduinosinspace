/* BooleanRequests
   This sketch demonstrates the two different ways to request
   boolean data from the game:

   - Supply an output pin. ArduinosInSpace will set this pin
     to OUTPUT mode. When the game sends a true for this request,
     the pin will be set to HIGH, and when the game sends a
     false, the pin will be set to LOW.
     This mode has an optional `invert` parameter to invert the
     logic.
   - Supply a callback function. When the game sends updates
     for this request, ArduinosInSpace will execute the callback,
     passing along the sent data.

   Hardware:
   - Two LEDs, each with a 220 Ohm resistor, attached to pins
     7 and 8. See schematics at https://www.arduino.cc/en/Tutorial/blink
     if you're unsure how to do this.
*/
#include "ArduinosInSpace.h"

const int IFFPin = 7;
const int enginePin = 8;

// We're going to request two channels:
ObjectsInSpace OIS(Serial, 2);

void setup() {
  Serial.begin(9600);
  OIS.begin();

  // Request IFF data from the game, and let ArduinosInSpace drive
  // the IFF LED directly. Note that we don't have to do any setup
  // of IFFPin at all, the library will manage it entirely.
  OIS.registerBool(IFF_ACTIVE, IFFPin);

  // But we do have to set the mode of our enginePin.
  pinMode(enginePin, OUTPUT);
  // Request data about the main engine. For this one we'll
  // provide a callback function, and manage the LED state in that.
  OIS.registerBool(MAIN_ENGINE_BURNING, engineCallback);

  OIS.activate();
}

void loop() {
  OIS.update();
}

// This is a simple callback that just changes a pin state.
// But we could use this instead to write to an LCD, change the
// colour of an RGB LED, send a message to another device, etc.
void engineCallback(int channel, bool data) {
  if (data) {
    digitalWrite(enginePin, HIGH);
  } else {
    digitalWrite(enginePin, LOW);
  }
}
