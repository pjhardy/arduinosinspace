/* TriggerDemo
   Demonstrating the trigger input handling of ArduinosInSpace.

   This demo uses a toggle switch to turn EMCON mode on or off.

   Hardware:
   A toggle switch, connected to pin 2 and the 5V pin, with a pull-down
   resistor. Refer to https://www.arduino.cc/en/tutorial/button for
   details and schematics.
*/
#include "ArduinosInSpace.h"

const int emconPin = 2;

// Create an ArduinosInSpace object that doesn't accept
// any data from the game, but does manage one input pin.
ObjectsInSpace OIS(Serial, 0, 1);

void setup() {
  Serial.begin(9600);
  OIS.begin();

  // Here we tell ArduinosInSpace to manage an input pin in trigger
  // mode. The first argument is the pin. The others are the commands
  // to send when the pin turns on and then off.
  OIS.registerTrigger(emconPin, EMCON_ON, EMCON_OFF);

  OIS.activate();
}

void loop() {
  OIS.update();
}
