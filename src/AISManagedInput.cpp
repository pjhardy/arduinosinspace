#include "AISManagedInput.h"

#include "AISUtil.h"

void AISManagedInputPin::poll() {
  // Our approach to debouncing is:
  // First state change triggers an event.
  // Ignore subsequent state changes for duration of debounce delay.
  uint8_t curState = digitalRead(_pin);
  if (curState != _debouncedState) {
    if ((millis() - _debouncedTime) > AISDebounceDelay) {
      _debouncedState = curState;
      _debouncedTime = millis();
      _sendCommand(curState);
    }
  }
}

AISTriggerInput::AISTriggerInput(uint8_t pin, OISCommand activeCommand,
				 OISCommand inactiveCommand, uint8_t mode,
				 ObjectsInSpaceInterface *serialCallback) {
  _pin = pin;
  _activeCommand = activeCommand;
  _inactiveCommand = inactiveCommand;
  // _mode should contain the value considered inactive.
  if (mode == INPUT_PULLUP) {
    _mode = HIGH;
  } else {
    _mode = LOW;
  }
  _debouncedTime = 0;
  _serialCallback = serialCallback;
}

void AISTriggerInput::_sendCommand(uint8_t newState) {
  if (_mode ^ newState == 0) {
    if (_inactiveCommand.channel > 0) {
      _serialCallback->sendCommand(_inactiveCommand);
    }
  } else {
    if (_activeCommand.channel > 0) {
      _serialCallback->sendCommand(_activeCommand);
    }
  }
}

AISLatchingInput::AISLatchingInput(uint8_t pin, OISCommand firstCommand,
				   OISCommand secondCommand, uint8_t mode,
				   ObjectsInSpaceInterface *serialCallback) {
  _pin = pin;
  _firstCommand = firstCommand;
  _secondCommand = secondCommand;
  // _mode should contain the value considered inactive.
  if (mode == INPUT_PULLUP) {
    _mode = HIGH;
  } else {
    _mode = LOW;
  }
  _debouncedTime = 0;
  _serialCallback = serialCallback;
}

void AISLatchingInput::_sendCommand(uint8_t newState) {
  if (_mode ^ newState == 0) {
    if (_lastCommandActive) {
      if (&_secondCommand) {
	_serialCallback->sendCommand(_secondCommand);
      }
      _lastCommandActive = false;
    } else {
      if (&_firstCommand) {
	_serialCallback->sendCommand(_firstCommand);
      }
      _lastCommandActive = true;
    }
  }
}
