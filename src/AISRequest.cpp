#include "AISRequest.h"

boolRequest::boolRequest(boolCallback callback)
{
  _callback = callback;
}

pinControlRequest::pinControlRequest(int outputPin, bool invert=false)
{
  pinMode(outputPin, OUTPUT);
  if (invert) {
    digitalWrite(outputPin, HIGH);
  } else {
    digitalWrite(outputPin, LOW);
  }
  _outputPin = outputPin;
  _invert = invert;
}

intRequest::intRequest(intCallback callback)
{
  _callback = callback;
}

floatRequest::floatRequest(floatCallback callback)
{
  _callback = callback;
}

rawRequest::rawRequest(rawCallback callback)
{
  _callback = callback;
}

void boolRequest::runCallback(int channel, int data)
{
  _callback(channel, data != 0);
}

void pinControlRequest::runCallback(int channel, int data)
{
  if ((data && !_invert) || (!data && _invert)) {
    digitalWrite(_outputPin, HIGH);
  } else {
    digitalWrite(_outputPin, LOW);
  }
}

void intRequest::runCallback(int channel, int data)
{
  _callback(channel, data);
}

void floatRequest::runCallback(int channel, int data)
{
  _callback(channel, data*0.01);
}

void rawRequest::runCallback(int channel, int data)
{
  _callback(channel, data);
}
