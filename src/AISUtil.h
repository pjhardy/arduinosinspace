/** @file AISUtil.h
    Contains common public structures and enums for other classes.
*/
#ifndef AISUtil_h
#define AISUtil_h

/** A struct representing an Objects in Space command.
    Contains information required to open a data channel or request
    a command channel. Each Arduinos in Space release includes constants
    covering all known commands and requests at time of release, so
    you shouldn't need to define your own.
*/
struct OISCommand {
  int channel; ///< The channel ID. Only used for command channels.
  char const * name; ///< The full text name of the request/command.
};

enum OISErrors {
  OISSerialOverflow,
  OISUnknownError
};

#endif
