#include "ArduinosInSpace.h"
#include "AISRequest.h"

ObjectsInSpace::ObjectsInSpace(Stream &serial, int numRequests,
			       int numCommandPins)
{
  _serial = &serial;
  _numRequests = numRequests;
  _commandCallbacks = new AISRequest*[numRequests];
  for (int i=0; i<numRequests; i++) {
    _commandCallbacks[i] = nullptr;
  }
  _numCommandPins = numCommandPins;
  _managedInputPins = new AISManagedInputPin*[numCommandPins];
  for (int i=0; i<numCommandPins; i++) {
    _managedInputPins[i] = nullptr;
  }
  _recvIdx = 0;
  _callbackidx = 0;
  _inputidx = 0;
  _streamState = handshaking;
}

ObjectsInSpace::ObjectsInSpace(Stream &serial, int numRequests):
  ObjectsInSpace::ObjectsInSpace(serial, numRequests, 0) {}

void ObjectsInSpace::begin()
{
  // TODO: Sending OIS SYN packets / waiting for ACK packets here.
  // return false if we got a NAK.
  while (_streamState == handshaking) {
    // Beta doesn't follow the docs. Hax.
    // _serial->print("SYN=1\n");
    _serial->print("451\n");
    delay(1000);
    update();
  }
}

// Channel names are long, and speed is not important for register
// commands. So we use multiple prints here to keep the required
// send buffer size small.
int ObjectsInSpace::registerCommand(OISCommand command)
{
  if (command.channel < 0 || command.channel > _maxCommand) {
    return -1;
  } else {
    _serial->print("CMD=");
    _serial->print(command.name);
    _serial->print(",");
    _serial->print(command.channel);
    _serial->print("\n");
    return 0;
  }
}

int ObjectsInSpace::registerTrigger(uint8_t pin, OISCommand activeCommand,
				    uint8_t mode=INPUT)
{
  if (_validateManagedInputs(activeCommand, NULL_COMMAND)) {
    pinMode(pin, mode);
    registerCommand(activeCommand);
    _managedInputPins[_inputidx] =
      new AISTriggerInput(pin, activeCommand, NULL_COMMAND, mode, this);
    _inputidx++;
    return (_inputidx-1);
  } else {
    return -1;
  }
}

int ObjectsInSpace::registerTrigger(uint8_t pin, OISCommand activeCommand,
				    OISCommand inactiveCommand,
				    uint8_t mode=INPUT)
{
  if (_validateManagedInputs(activeCommand, inactiveCommand)) {
    pinMode(pin, mode);
    registerCommand(activeCommand);
    registerCommand(inactiveCommand);
    _managedInputPins[_inputidx] =
      new AISTriggerInput(pin, activeCommand, inactiveCommand, mode, this);
    _inputidx++;
    return (_inputidx-1);
  } else {
    return -1;
  }
}

int ObjectsInSpace::registerLatching(uint8_t pin, OISCommand firstCommand,
				     OISCommand secondCommand,
				     uint8_t mode=INPUT)
{
  if (_validateManagedInputs(firstCommand, secondCommand)) {
    pinMode(pin, mode);
    registerCommand(firstCommand);
    registerCommand(secondCommand);
    _managedInputPins[_inputidx] = new AISLatchingInput(pin, firstCommand,
							secondCommand, mode,
							this);
    _inputidx++;
    return (_inputidx-1);
  } else {
    return -1;
  }
}

int ObjectsInSpace::registerBool(OISCommand command, boolCallback callback)
{
  if (_validateCommand(command)) {
    _sendRequest(boolRequestType, command);
    _commandCallbacks[_callbackidx] = new boolRequest(callback);
    _callbackidx++;
    return (_callbackidx-1);
  } else {
    return -1;
  }
}

int ObjectsInSpace::registerBool(OISCommand command, int pin, bool invert=false)
{
  if (_validateCommand(command)) {
    _sendRequest(boolRequestType, command);
    _commandCallbacks[_callbackidx] = new pinControlRequest(pin, invert);
    _callbackidx++;
    return (_callbackidx-1);
  } else {
    return -1;
  }
}

int ObjectsInSpace::registerInt(OISCommand command, intCallback callback)
{
  if (_validateCommand(command)) {
    _sendRequest(intRequestType, command);
    _commandCallbacks[_callbackidx] = new intRequest(callback);
    _callbackidx++;
    return (_callbackidx-1);
  } else {
    return -1;
  }
}

int ObjectsInSpace::registerFloat(OISCommand command, floatCallback callback)
{
  if (_validateCommand(command)) {
    _sendRequest(floatRequestType, command);
    _commandCallbacks[_callbackidx] = new floatRequest(callback);
    _callbackidx++;
    return (_callbackidx-1);
  } else {
    return -1;
  }
}

int ObjectsInSpace::registerBoolRaw(OISCommand command, rawCallback callback)
{
  if (_validateCommand(command)) {
    _sendRequest(boolRequestType, command);
    _commandCallbacks[_callbackidx] = new rawRequest(callback);
    _callbackidx++;
    return (_callbackidx-1);
  } else {
    return -1;
  }
}

int ObjectsInSpace::registerIntRaw(OISCommand command, rawCallback callback)
{
  if (_validateCommand(command)) {
    _sendRequest(intRequestType, command);
    _commandCallbacks[_callbackidx] = new rawRequest(callback);
    _callbackidx++;
    return (_callbackidx-1);
  } else {
    return -1;
  }
}

int ObjectsInSpace::registerFloatRaw(OISCommand command, rawCallback callback)
{
  if (_validateCommand(command)) {
    _sendRequest(floatRequestType, command);
    _commandCallbacks[_callbackidx] = new rawRequest(callback);
    _callbackidx++;
    return (_callbackidx-1);
  } else {
    return -1;
  }
}

float ObjectsInSpace::toFloat(int data)
{
  return (data * 0.01);
}

void ObjectsInSpace::activate()
{
  _serial->print("ACT\n");
  // TODO: Need a better way to send the version number here.
  // Constant in AISUtil.h?
  _serial->print("DBG=ArduinosInSpace version 1.2 device successfully connected!\n");
  _streamState = active;
}

void ObjectsInSpace::sendCommand(OISCommand command)
{
  snprintf(_sendBuffer, _sendBufSize, "EXC=%i\n", command.channel);
  _serial->print(_sendBuffer);
}

void ObjectsInSpace::sendDebug(char* message)
{
  _serial->print("DBG=");
  _serial->print(message);
  _serial->print("\n");
}

void ObjectsInSpace::update()
{
  while (_serial->available() > 0) {
    uint8_t nextChar = _serial->read();
    if (nextChar == '\n') {
      _recvBuffer[min(_recvIdx, (_recvBufSize-1))] = 0;
      _parseSerial();
      _recvIdx = 0;
    } else {
      if (_recvIdx+1 < _recvBufSize) {
	_recvBuffer[_recvIdx] = nextChar;
	_recvIdx++;
      } else {
	// Error, avoiding buffer overflow
      }
    }
  }
  // TODO: Consider polling at set intervals, rather than every update.
  for (int i=0; i<_numCommandPins; i++) {
    if (_managedInputPins[i] == nullptr) {
      break;
    } else {
      _managedInputPins[i]->poll();
    }
  }
}

void ObjectsInSpace::_parseSerial()
{
  switch (_streamState) {
  case active:
    int commandIndex;
    int payload;
    char *token;
    token = strtok(_recvBuffer, "=");
    commandIndex = atoi(token);
    token = strtok(NULL, "=");
    payload = atoi(token);

    // Execute the callback indicated by commandIndex.
    // Pass it payload.
    if (commandIndex >= 0 && commandIndex <= _numRequests &&
	_commandCallbacks[commandIndex] != nullptr) {
      _commandCallbacks[commandIndex]->runCallback(commandIndex, payload);
    } else {
      // Error: Invalid data received
    }
    break;
  case syncing:
    // Nothing to do here
    break;
  case handshaking:
    // Beta doesn't follow docs, should be ACK
    if (strcmp(_recvBuffer, "452\r")) {
      _streamState = syncing;
    } else if (strcmp(_recvBuffer, "DEN")) {
      // TODO: error condition?
    }
  }
}

// These validate commands have morphed in to a dumping ground for
// all manner of things related to ensuring we can add a new command,
// not just whether the command exists.
bool ObjectsInSpace::_validateManagedInputs(OISCommand firstCommand,
					   OISCommand secondCommand)
{
  if (firstCommand.channel < 0 || firstCommand.channel > _maxCommand) {
    return false;
  } else if (secondCommand.channel < 0 || secondCommand.channel > _maxCommand) {
    return false;
  } else if (_inputidx >= _numCommandPins) {
    return false;
  } else {
    return true;
  }
}

bool ObjectsInSpace::_validateCommand(OISCommand command)
{
  if (command.channel < 0 || command.channel > _maxCommand) {
    return false;
  } else if (_callbackidx >= _numRequests) {
    return false;
  } else {
    return true;
  }
}

void ObjectsInSpace::_sendRequest(OISRequest request, OISCommand command)
{
  switch(request) {
  case boolRequestType:
    _serial->print("NIB=");
    break;
  case intRequestType:
    _serial->print("NIN=");
    break;
  case floatRequestType:
    _serial->print("NIF=");
    break;
  default:
    return;
  }
  _serial->print(command.name);
  _serial->print(",");
  _serial->print(_callbackidx);
  _serial->print("\n");
}

#ifdef OIS_DEBUG
void errorHandler(OISErrorCallback callback)
{
  _errorCallback = callback;
}
#endif
