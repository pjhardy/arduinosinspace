#ifndef ArduinosInSpace_h
#define ArduinosInSpace_h

#ifndef Arduino_h
#include <Arduino.h>
#endif

#include "AISRequest.h"
#include "AISManagedInput.h"
#include "AISUtil.h"
#include "ArduinosInSpaceRequests.h"

/** The ObjectsInSpace object.
    This is the main interface for ArduinosInSpace. A single
    ObjectsInSpace object managees all aspects of a serial connection.
    It provides an interface to send commands to the game, and can
    poll input pins to trigger these commands. It also allows to
    request data from the game, and will pass received data to
    a specified output pin, or a callback function for more complex
    requests.
*/
class ObjectsInSpace : public ObjectsInSpaceInterface
{
 public:
  /** The ObjectsInSpace constructor.
      Creates an ObjectsInSpace object to manage a serial connection.
      Note that this constructor *will not* attempt to start the
      serial interface.
      @param serial The serial instance this instance will use to
      communicate with the game. Usually "Serial".
      @param numRequests The number of request channels we'll allocate.
      @param numCommandPins The number of input pins we'll manage. Note
      that this does not include commands specified with
      `registerCommand()`.
  */
  ObjectsInSpace(Stream &serial, int numRequests, int numCommandPins);
  /** Deprecated constructor.
      This constructor is deprecated and will be removed in a future
      release.
      @param serial The serial instance this instance will use to
      communicate with the game. Usually "Serial".
      @param numRequests The number of request channels we'll allocate.
  */
  ObjectsInSpace(Stream &serial, int numRequests);

  typedef void (*OISErrorCallback)(OISErrors error);

  /* Handshaking methods */
  /** Initialise the connection, and perform handshaking with the game.
      The begin() function will periodically send a handshake request
      packet, and wait for the game to send a successful response.
      This is a blocking function - it won't return until handshaking
      is successful and the game has moved to the Synchronisation
      phase of the connection.
  */
  void begin();

  /* Syncing */
  /** Register a client command.
      Before the game will accept a given command, it must be registered.
      This function will register the command.
      Arduinos In Space includes constants for all known commands listed
      on the unofficial OiS wiki at
      https://oiswiki.sysadninjas.net/wiki/Serial_Commands
      @param command The command constant to register.
      
  */
  int registerCommand(OISCommand command);
  /** Register a client command and bind it to an input pin.
      ArduinosInSpace will poll this pin, and whenever it changes
      from inactive to active will send the given client command.
      The maximum number of managed command pins are specified by
      the numCommandPins argument when constructing an ObjectsInSpace
      object. Attempts to register more than this limit will silently fail.
      Arduinos In Space includes constants for all known commands listed
      on the unofficial OiS wiki at
      https://oiswiki.sysadninjas.net/wiki/Serial_Commands
      @param pin The pin to monitor. ArduinosInSpace will set the mode.
      @param activeCommand The command to send when the pin goes active
      @param mode This sets the pin mode, as well as what readings will
      be considered active. When set to INPUT, the pin is active when HIGH.
      When set to INPUT_PULLUP, the pin is active when LOW.
      @returns The index of this command in the internal command list.
  */
  int registerTrigger(uint8_t pin, OISCommand activeCommand,
		      uint8_t mode=INPUT);
  /** Register two commands bound to an input pin.
      ArduinosInSpace will poll this pin, and send activeCommand when the
      pin changes from inactive to active, and inactiveCommand when the
      pin changes from active to inactive.
      The maximum number of managed command pins are specified by
      the numCommandPins argument when constructing an ObjectsInSpace
      object. Attempts to register more than this limit will silently fail.
      Arduinos In Space includes constants for all known commands listed
      on the unofficial OiS wiki at
      https://oiswiki.sysadninjas.net/wiki/Serial_Commands
      @param pin The pin to monitor. ArduinosInSpace will set the mode.
      @param activeCommand The command to send when the pin goes active.
      @param inactiveCommand The command to send when the pin goes inactive.
      @param mode This sets the pin mode, as well as what readings will
      be considered active. When set to INPUT, the pin is active when HIGH.
      When set to INPUT_PULLUP, the pin is active when LOW.
      @returns The index of this command in the internal command list.
  */
  int registerTrigger(uint8_t pin, OISCommand activeCommand,
		      OISCommand inactiveCommand, uint8_t mode=INPUT);
  /** Register two commands bound to an input pin in latching mode.
      ArduinosInSpace will poll this pin. The first time it switches
      from inactive to active the first command will be sent. The
      second time it switches from inactive to active the second command
      will be sent, and so on.
      The maximum number of managed command pins are specified by
      the numCommandPins argument when constructing an ObjectsInSpace
      object. Attempts to register more than this limit will silently fail.
      Arduinos In Space includes constants for all known commands listed
      on the unofficial OiS wiki at
      https://oiswiki.sysadninjas.net/wiki/Serial_Commands
      @param pin The pin to monitor. ArduinosInSpace will set the mode.
      @param firstCommand The command to send first active change.
      @param secondCommand The command to send second active change.
      @param active Thsi sets the pin mode, as well as what readings will
      be considered active. When set to INPUT, the pin is active when HIGH.
      When set to INPUT_PULLUP, the pin is active when LOW.
      @returns The index of this command in the internal command list.
  */
  int registerLatching(uint8_t pin, OISCommand firstCommand,
		       OISCommand secondCommand, uint8_t mode=INPUT);
  /** Register a request for boolean data.
      This function will create a channel to receive the requested
      boolean data from the game. When data on this channel is received,
      Arduinos In Space will convert it to a boolean data type (true/false),
      and then pass it as an argument to the given callback function.
      The maximum number of request channels that can be created is
      specified by the numRequests argument to the ObjectsInSpace
      constructor. Attempts to request more than this limit will
      silently fail.
      Arduinos In Space includes constants for all known requests listed
      on the unofficial OiS wiki at
      https://oiswiki.sysadninjas.net/wiki/Serial_Requests
      @param command The data command to request.
      @param callback A callback function to handle the received data.
      @returns The index of this request in the internal request list.
  */
  int registerBool(OISCommand command, boolCallback callback);
  /** Register a request for boolean data.
      This function will create a channel to receive the requested
      boolean data from the game. When data on this channel is received,
      Arduinos in Space will change the status of the given output pin
      to match the data received. Note that this function also sets the
      pin mode appropriately, there is no need for a separate `pinMode()`
      call.
      The maximum number of request channels that can be created is
      specified by the numRequests argument to the ObjectsInSpace
      constructor. Attempts to request more than this limit will
      silently fail.
      Arduinos In Space includes constants for all known requests listed
      on the unofficial OiS wiki at
      https://oiswiki.sysadninjas.net/wiki/Serial_Requests
      @param command The data command to request.
      @param pin A pin number to control.
      @param invert By default, the output pin will be set high if a
      boolean true is received on this channel, and low if a boolean false
      is received. Set the optional invert parameter to true to invert
      this logic.
  */
  int registerBool(OISCommand command, int pin, bool invert=false);
  /** Register a request for numeric data.
      This function will create a channel to receive the requested integer
      data from the game. When data on this channel is received,
      Arduinos In Space will pass the data in integer format as an
      argument to the supplied callback function.
      The maximum number of request channels that can be created is
      specified by the numRequests argument to the ObjectsInSpace
      constructor. Attempts to request more than this limit will
      silently fail.
      Arduinos In Space includes constants for all known requests listed
      on the unofficial OiS wiki at
      https://oiswiki.sysadninjas.net/wiki/Serial_Requests
      @param command The data command to request.
      @param callback A callback function to handle the received data.
      @param invert If set to true, the pin state is inverted
  */
  int registerInt(OISCommand command, intCallback callback);
  /** Register a request for floating point data.
      This function will create a channel to receive the requested
      floating point data from the game. When data on this channel is
      received, Arduinos In Space will convert it to a floating point
      type (`float`), before passing it as an argument to the supplied
      callback function.
      The maximum number of request channels that can be created is
      specified by the numRequests argument to the ObjectsInSpace
      constructor. Attempts to request more than this limit will
      silently fail.
      Arduinos In Space includes constants for all known requests listed
      on the unofficial OiS wiki at
      https://oiswiki.sysadninjas.net/wiki/Serial_Requests
      @param command The data command to request.
      @param callback A callback function to handle the received data.
  */
  int registerFloat(OISCommand command, floatCallback callback);
  /** Register a request for boolean data.
      This method is identical to the `registerBool()` method, but
      does not process data before passing it to the callback.
      The maximum number of request channels that can be created is
      specified by the numRequests argument to the ObjectsInSpace
      constructor. Attempts to request more than this limit will
      silently fail.
      Arduinos In Space includes constants for all known requests listed
      on the unofficial OiS wiki at
      https://oiswiki.sysadninjas.net/wiki/Serial_Requests
      @param command The data to request.
      @param callback A callback function to handle the received data.
  */
  int registerBoolRaw(OISCommand command, rawCallback callback);
  /** Register a request for numeric data.
      This method is identical to the `registerInt()` method, but
      does not process data before passing it to the callback.
      The maximum number of request channels that can be created is
      specified by the numRequests argument to the ObjectsInSpace
      constructor. Attempts to request more than this limit will
      silently fail.
      Arduinos In Space includes constants for all known requests listed
      on the unofficial OiS wiki at
      https://oiswiki.sysadninjas.net/wiki/Serial_Requests
      @param command The data to request.
      @param callback A callback function to handle the received data.
  */
  int registerIntRaw(OISCommand command, rawCallback callback);
  /** Register a request for floating point data.
      This method is identical to the `registerFloat()` method, but
      does not process data before passing it to the callback.
      The maximum number of request channels that can be created is
      specified by the numRequests argument to the ObjectsInSpace
      constructor. Attempts to request more than this limit will
      silently fail.
      Arduinos In Space includes constants for all known requests listed
      on the unofficial OiS wiki at
      https://oiswiki.sysadninjas.net/wiki/Serial_Requests
      @param command The data to request.
      @param callback A callback function to handle the received data.
  */
  int registerFloatRaw(OISCommand command, rawCallback callback);
  /** Start active phase.
      This function tells the game that we have finished setting up commands
      and channels, and are ready to start sending and receiving game data.
  */
  void activate();

  /* Active phase */
  /** Send a command to the game. See the protocol documentation for a
      full list of possible commands.
      @param command The command constant to send.
  */
  void sendCommand(OISCommand command);
  /** Send a log message to the game. The game will treat this as a debug
      log, and add it to the game log file.
      @param message A string to be logged.
  */
  void sendDebug(char* message);
  /** Helper function to convert data received from the game to
      a floating point number.
  */
  float toFloat(int data);
  /** The main ObjectsInSpace loop. Call this regularly in your loop()
      function.
  */
  void update();
  #ifdef OIS_DEBUG
  /** An optional callback to receive error messages. To use this, add
      `#define OIS_DEBUG` to the top of your sketch, and create an
      OISErrorCallback function that will receive and process errors.
  */
  void errorHandler(OISErrorCallback callback);
  #endif
 private:
  // Put a ceiling on the channel ID, as a dirty way to bounds
  // check our input.
  // TODO: Maybe a better way to do this?
  static const int _maxCommand = 500;

  // Convenience enum for tracking the state of the connection.
  enum OISState {
    handshaking,
    syncing,
    active
  };

  // The possible request types.
  enum OISRequest {
    boolRequestType,
    intRequestType,
    floatRequestType
  };

  // Stores the maximum number of request channels
  int _numRequests;
  // Stores the maximum number of managed input pins
  int _numCommandPins;

  // Our serial device
  Stream *_serial;

  OISState _streamState;
  // This will hold an array of callback functions
  AISRequest** _commandCallbacks;
  // This will hold an array of managed input commands
  AISManagedInputPin** _managedInputPins;

  // Tracks _commandCallbacks index while filling the array in Syncing phase.
  int _callbackidx;
  // Tracks _managedInputPins index while filling the array in Syncing phase.
  int _inputidx;

  // Serial buffer sizes
  static const int _sendBufSize = 10;
  static const int _recvBufSize = 16;
  // Serial buffers
  char _sendBuffer[_sendBufSize];
  char _recvBuffer[_recvBufSize];
  // Tracks current position reading in to _recvBuffer
  int _recvIdx;

  bool _validateManagedInputs(OISCommand firstCommand,
			      OISCommand secondCommand);
  bool _validateCommand(OISCommand command);
  void _sendRequest(OISRequest request, OISCommand command);

  void _parseSerial();


  #ifdef OIS_DEBUG
  OISErrorCallback _errorCallback;
  #endif
};

#endif
