#ifndef AISManagedInput_h
#define AISManadedInput_h

#include <Arduino.h>

#include "AISUtil.h"

const int AISDebounceDelay = 10;

// Defining this interface, and then having the ObjectsInSpace
// class implement that interface, is how we allow managed input
// objects to store a reference to ObjectsInSpace objects.
// And that's how we allow managed input objects direct access to
// the public methods, so they can send commands the same way
// the user would.
class ObjectsInSpaceInterface
{
 public:
  virtual void sendCommand(OISCommand command) = 0;
};

class AISManagedInputPin
{
 public:
  void poll();
 protected:
  uint8_t _pin;
  uint8_t _mode;
  uint8_t _debouncedState;
  unsigned long _debouncedTime;
  ObjectsInSpaceInterface *_serialCallback;
  virtual void _sendCommand(uint8_t newState)=0;
};

class AISTriggerInput: public AISManagedInputPin
{
 public:
  AISTriggerInput(uint8_t pin, OISCommand activeCommand,
		  uint8_t mode, ObjectsInSpaceInterface *serialCallback);
  AISTriggerInput(uint8_t pin, OISCommand activeCommand,
		  OISCommand inactiveCommand, uint8_t mode,
		  ObjectsInSpaceInterface *serialCallback);
 protected:
  OISCommand _activeCommand;
  OISCommand _inactiveCommand;
  void _sendCommand(uint8_t newState);
};

class AISLatchingInput: public AISManagedInputPin
{
 public:
  AISLatchingInput(uint8_t pin, OISCommand firstCommand,
		   OISCommand secondCommand, uint8_t mode,
		   ObjectsInSpaceInterface *serialCallback);
  void sendCommand(uint8_t newState);
 protected:
  OISCommand _firstCommand;
  OISCommand _secondCommand;
  bool _lastCommandActive;
  void _sendCommand(uint8_t newState);
};

#endif
