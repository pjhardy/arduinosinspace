#ifndef AISRequest_h
#define AISRequest_h

#include <Arduino.h>

typedef void (*boolCallback)(int channel, bool payload);
typedef void (*intCallback)(int channel, int payload);
typedef void (*floatCallback)(int channel, float payload);
typedef void (*rawCallback)(int channel, int payload);

class AISRequest
{
 public:
  virtual void runCallback(int channel, int data)=0;
};

class boolRequest: public AISRequest {
 public:
  boolRequest(boolCallback callback);
  void runCallback(int channel, int data);
 protected:
  boolCallback _callback;
};

class pinControlRequest: public AISRequest {
 public:
  pinControlRequest(int outputPin, bool invert=false);
  void runCallback(int channel, int data);
 protected:
  int _outputPin;
  bool _invert;
};

class intRequest: public AISRequest {
 public:
  intRequest(intCallback callback);
  void runCallback(int channel, int data);
 protected:
  intCallback _callback;
};

class floatRequest: public AISRequest {
 public:
  floatRequest(floatCallback callback);
  void runCallback(int channel, int data);
 protected:
  floatCallback _callback;
};

class rawRequest: public AISRequest {
 public:
  rawRequest(rawCallback callback);
  void runCallback(int channel, int data);
 protected:
  rawCallback _callback;
};

#endif
